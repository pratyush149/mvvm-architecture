package org.aisle.application.base

import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication
import org.aisle.application.di.DaggerApplicationComponent


class BaseApplication : DaggerApplication() {


    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        val component = DaggerApplicationComponent.builder().application(this).build()
        component.inject(this)

        return component
    }

}