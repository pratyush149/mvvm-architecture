package org.aisle.application.ui.views.main

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.NonNull
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.fragment_discover.*
import org.aisle.application.R
import org.aisle.application.base.BaseFragment
import org.aisle.application.databinding.FragmentDiscoverBinding
import org.aisle.application.ui.viewmodel.MainViewModel
import org.aisle.application.utils.AppUtil
import org.aisle.application.utils.NetworkUtil


class DiscoverFragment : BaseFragment<MainViewModel>() {
    lateinit var binding: FragmentDiscoverBinding
    private lateinit var mContext: Context
    private lateinit var interestsRecyclerAdapter: InterestsRecyclerAdapter

    override fun layoutRes(): Int {
        return R.layout.fragment_discover
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
    }

    override fun getViewModelType(): Class<MainViewModel> {
        return MainViewModel::class.java
    }

    override fun onCreateView(
        @NonNull inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = FragmentDiscoverBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.lifecycleOwner = this
        binding.viewmodel = viewModel

        initViewModelObservers()

        getProfileList()

        setupRecycler()
    }

    private fun setupRecycler() {

        rvInterest.layoutManager =
            LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false)
        interestsRecyclerAdapter = InterestsRecyclerAdapter(mContext)
        rvInterest.adapter = interestsRecyclerAdapter
    }

    private fun getProfileList() {
        if (NetworkUtil.isNetworkAvailable(mContext)) {
            viewModel.getProfileList()
        }
    }

    private fun initViewModelObservers() {
        viewModel.getLoadingData().observe(viewLifecycleOwner, Observer {
            if (it) {
                showProgressDialog(mContext)
            } else {
                dismissProgressDialog()
            }
        })

        viewModel.getProfileData().observe(viewLifecycleOwner, Observer {
        })

        viewModel.getErrorData().observe(viewLifecycleOwner, Observer {
            AppUtil.showSnackBar(clLogin, mContext, it)
        })
    }

}