package org.aisle.application.ui.views.main

import android.content.Context
import android.os.Bundle
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import org.aisle.application.R
import org.aisle.application.base.BaseActivity
import org.aisle.application.databinding.ActivityMainBinding
import org.aisle.application.ui.viewmodel.MainViewModel

class MainActivity : BaseActivity<MainViewModel, ActivityMainBinding>() {

    lateinit var mContext: Context
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.viewmodel = viewModel
        mContext = this

        setUpBottomNavigationView()

    }


    private fun setUpBottomNavigationView() {
        val navView: BottomNavigationView = findViewById(R.id.bottom_nav)

        val navController = findNavController(R.id.nav_host_fragment)
        navView.setupWithNavController(navController)
    }

    override fun layoutRes(): Int {
        return R.layout.activity_main
    }

    override fun getViewModelType(): Class<MainViewModel> {
        return MainViewModel::class.java
    }
}