package org.aisle.application.ui.views.main

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import org.aisle.application.R
import org.aisle.application.model.response.main.Profile__1

class InterestsRecyclerAdapter(val context: Context) :
    RecyclerView.Adapter<InterestsRecyclerAdapter.InterestViewHolder>() {



    class InterestViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var ivInterests: ImageView

        init {
            ivInterests = itemView.findViewById(R.id.ivInterests)
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InterestViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_interests, parent, false)
        return InterestViewHolder(view)
    }

    override fun onBindViewHolder(holder: InterestViewHolder, position: Int) {

    }

    override fun getItemCount(): Int {
        return 5
    }
}