package org.aisle.application.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import org.aisle.application.data.repository.MainRepository
import org.aisle.application.model.response.main.MainResponse
import org.aisle.application.utils.Session
import retrofit2.Response
import java.net.HttpURLConnection
import javax.inject.Inject

class MainViewModel @Inject constructor(private val mainRepository: MainRepository) : ViewModel() {

    private var disposable: CompositeDisposable? = CompositeDisposable()
    private var profileData: MutableLiveData<MainResponse> = MutableLiveData()
    private var repoLoadError: MutableLiveData<String> = MutableLiveData()
    private var loading: MutableLiveData<Boolean> = MutableLiveData()

    @Inject
    lateinit var session: Session

    fun getProfileList() {
        loading.value = true
        disposable?.add(
            mainRepository.getProfileList(session.getToken()!!)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<Response<MainResponse>>() {
                    override fun onSuccess(response: Response<MainResponse>?) {
                        loading.value = false
                        if (response != null && response.code() == HttpURLConnection.HTTP_OK) {
                            profileData.value = response.body()
                        } else {
                            repoLoadError.value = response!!.message()
                        }
                    }

                    override fun onError(e: Throwable?) {
                        loading.value = false
                        repoLoadError.value = e!!.message
                    }
                })
        )
    }

    fun getProfileData(): LiveData<MainResponse> {
        return profileData
    }


    fun getErrorData(): LiveData<String> {
        return repoLoadError
    }

    fun getLoadingData(): LiveData<Boolean> {

        return loading
    }

    override fun onCleared() {
        disposable?.dispose()
        super.onCleared()
    }

}