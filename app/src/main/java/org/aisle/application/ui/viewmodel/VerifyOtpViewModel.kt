package org.aisle.application.ui.viewmodel

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import org.aisle.application.data.repository.VerifyOtpRepository
import org.aisle.application.model.request.VerifyOtpRequestBody
import org.aisle.application.model.response.otp.VerifyOtpResponse
import retrofit2.Response
import java.net.HttpURLConnection
import javax.inject.Inject


class VerifyOtpViewModel @Inject constructor(private val verifyOtpRepository: VerifyOtpRepository) :
    ViewModel() {

    private var disposable: CompositeDisposable? = CompositeDisposable()
    private var verifyOtpResponseData = MutableLiveData<VerifyOtpResponse>()
    private var repoLoadError: MutableLiveData<String> = MutableLiveData()
    private var validate: MutableLiveData<Boolean> = MutableLiveData()
    private var loading: MutableLiveData<Boolean> = MutableLiveData()

    val otp: ObservableField<String> = ObservableField("")

    fun verifyOtp(
    ) {
        loading.value = true
        disposable?.add(
            verifyOtpRepository.verifyOtp(VerifyOtpRequestBody("+919876543212","1234"))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<Response<VerifyOtpResponse>>() {
                    override fun onSuccess(response: Response<VerifyOtpResponse>?) {
                        loading.value = false
                        if (response != null && response.code() == HttpURLConnection.HTTP_OK) {
                            verifyOtpResponseData.value = response.body()
                        } else {
                            repoLoadError.value = response?.message()
                        }
                    }

                    override fun onError(e: Throwable?) {
                        loading.value = false
                        repoLoadError.value = e?.message
                    }
                })
        )
    }


    fun validateOtp() {
        if (otp.get()?.isEmpty() == true|| otp.get()?.length!! < 4) {
            validate.value = false
            repoLoadError.value = "Please Enter Valid OTP"
            return
        }

        validate.value = true

    }


    fun getValidateLogin(): LiveData<Boolean> {

        return validate
    }

    fun getVerifyOtpData(): LiveData<VerifyOtpResponse> {
        return verifyOtpResponseData
    }

    fun getLoadingData(): LiveData<Boolean> {

        return loading
    }


    fun getErrorData(): LiveData<String> {
        return repoLoadError
    }


    override fun onCleared() {
        disposable?.dispose()
        super.onCleared()
    }
}