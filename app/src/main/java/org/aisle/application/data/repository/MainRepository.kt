package org.aisle.application.data.repository

import io.reactivex.Single
import org.aisle.application.data.webservice.RestService
import org.aisle.application.model.response.main.MainResponse
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MainRepository @Inject constructor() {

    @Inject
    lateinit var restService: RestService


    fun getProfileList(token: String): Single<Response<MainResponse>> {
        return restService.getProfileList(token)
    }

}
