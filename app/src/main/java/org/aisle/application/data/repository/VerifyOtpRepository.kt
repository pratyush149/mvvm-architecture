package org.aisle.application.data.repository

import io.reactivex.Single
import org.aisle.application.data.webservice.RestService
import org.aisle.application.model.request.VerifyOtpRequestBody
import org.aisle.application.model.response.otp.VerifyOtpResponse
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class VerifyOtpRepository @Inject constructor() {

    @Inject
    lateinit var restService: RestService


    fun verifyOtp(
        verifyOtpRequestBody: VerifyOtpRequestBody
    ): Single<Response<VerifyOtpResponse>> {
        return restService.verifyOtp(verifyOtpRequestBody)
    }

}