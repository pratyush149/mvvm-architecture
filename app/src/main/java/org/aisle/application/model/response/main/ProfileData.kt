package org.aisle.application.model.response.main

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName
import javax.annotation.Generated


@Generated("jsonschema2pojo")
data class ProfileData (
    @SerializedName("question")
    @Expose
    var question: String? = null,

    @SerializedName("preferences")
    @Expose
    var preferences: List<Preference__1>? = null,

    @SerializedName("invitation_type")
    @Expose
    var invitationType: String? = null
    )