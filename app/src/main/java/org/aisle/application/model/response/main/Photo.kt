package org.aisle.application.model.response.main

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName
import javax.annotation.Generated


@Generated("jsonschema2pojo")
class Photo {
    @SerializedName("photo")
    @Expose
    var photo: String? = null

    @SerializedName("photo_id")
    @Expose
    var photoId: Int? = null

    @SerializedName("selected")
    @Expose
    var selected: Boolean? = null

    @SerializedName("status")
    @Expose
    var status: Any? = null
}