package org.aisle.application.model.response.main

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName
import javax.annotation.Generated


@Generated("jsonschema2pojo")
data class MainResponse (
    @SerializedName("invites")
    @Expose
    var invites: Invites? = null,

    @SerializedName("likes")
    @Expose
    var likes: Likes? = null
    )