package org.aisle.application.model.response.main

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName
import javax.annotation.Generated


@Generated("jsonschema2pojo")
data class Location(
    @SerializedName("summary")
    @Expose
    var summary: String? = null,

    @SerializedName("full")
    @Expose
    var full: String? = null
)