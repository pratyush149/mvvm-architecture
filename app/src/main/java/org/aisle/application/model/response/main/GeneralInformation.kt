package org.aisle.application.model.response.main

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName
import javax.annotation.Generated


@Generated("jsonschema2pojo")
class GeneralInformation (
    @SerializedName("date_of_birth")
    @Expose
    var dateOfBirth: String? = null,

    @SerializedName("date_of_birth_v1")
    @Expose
    var dateOfBirthV1: String? = null,

    @SerializedName("location")
    @Expose
    var location: Location? = null,

    @SerializedName("drinking")
    @Expose
    var drinking: String? = null,

    @SerializedName("drinking_v1")
    @Expose
    var drinkingV1: Drinking? = null,

    @SerializedName("first_name")
    @Expose
    var firstName: String? = null,

    @SerializedName("gender")
    @Expose
    var gender: String? = null,

    @SerializedName("marital_status")
    @Expose
    var maritalStatus: String? = null,

    @SerializedName("marital_status_v1")
    @Expose
    var maritalStatusV1: MaritalStatus? = null,

    @SerializedName("ref_id")
    @Expose
    var refId: String? = null,

    @SerializedName("smoking")
    @Expose
    var smoking: String? = null,

    @SerializedName("smoking_v1")
    @Expose
    var smokingV1: Smoking? = null,

    @SerializedName("sun_sign")
    @Expose
    var sunSign: String? = null,

    @SerializedName("sun_sign_v1")
    @Expose
    var sunSignV1: SunSign? = null,

    @SerializedName("mother_tongue")
    @Expose
    var motherTongue: MotherTongue? = null,

    @SerializedName("faith")
    @Expose
    var faith: Faith? = null,

    @SerializedName("height")
    @Expose
    var height: Int? = null,

    @SerializedName("cast")
    @Expose
    var cast: Any? = null,

    @SerializedName("kid")
    @Expose
    var kid: Any? = null,

    @SerializedName("diet")
    @Expose
    var diet: Any? = null,

    @SerializedName("politics")
    @Expose
    var politics: Any? = null,

    @SerializedName("pet")
    @Expose
    var pet: Any? = null,

    @SerializedName("settle")
    @Expose
    var settle: Any? = null,

    @SerializedName("age")
    @Expose
    var age: Int? = null
    )