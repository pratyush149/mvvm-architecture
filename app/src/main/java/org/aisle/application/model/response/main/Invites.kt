package org.aisle.application.model.response.main

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName
import javax.annotation.Generated


@Generated("jsonschema2pojo")
class Invites(
    @SerializedName("profiles")
    @Expose
    var profiles: List<Profile>? = null,

    @SerializedName("totalPages")
    @Expose
    var totalPages: Int? = null,

    @SerializedName("pending_invitations_count")
    @Expose
    var pendingInvitationsCount: Int? = null
)