package org.aisle.application.model.response.main

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName
import javax.annotation.Generated


@Generated("jsonschema2pojo")
data class Work(
    @SerializedName("industry")
    @Expose
    var industry: String? = null,

    @SerializedName("industry_v1")
    @Expose
    var industryV1: Industry? = null,

    @SerializedName("monthly_income_v1")
    @Expose
    var monthlyIncomeV1: Any? = null,

    @SerializedName("experience")
    @Expose
    var experience: String? = null,

    @SerializedName("experience_v1")
    @Expose
    var experienceV1: Experience? = null,

    @SerializedName("highest_qualification")
    @Expose
    var highestQualification: String? = null,

    @SerializedName("highest_qualification_v1")
    @Expose
    var highestQualificationV1: HighestQualification? = null,

    @SerializedName("field_of_study")
    @Expose
    var fieldOfStudy: String? = null,

    @SerializedName("field_of_study_v1")
    @Expose
    var fieldOfStudyV1: FieldOfStudyV1? = null
)