package org.aisle.application.model.response.main

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import javax.annotation.Generated


@Generated("jsonschema2pojo")
data class Profile (
    @SerializedName("general_information")
    @Expose
    var generalInformation: GeneralInformation? = null,

    @SerializedName("approved_time")
    @Expose
    var approvedTime: Double? = null,

    @SerializedName("photos")
    @Expose
    var photos: List<Photo>? = null,

    @SerializedName("user_interests")
    @Expose
    var userInterests: List<Any>? = null,

    @SerializedName("work")
    @Expose
    var work: Work? = null,

    @SerializedName("preferences")
    @Expose
    var preferences: List<Preference>? = null,

    @SerializedName("instagram_images")
    @Expose
    var instagramImages: Any? = null,

    @SerializedName("last_seen_window")
    @Expose
    var lastSeenWindow: String? = null,

    @SerializedName("is_facebook_data_fetched")
    @Expose
    var isFacebookDataFetched: Boolean? = null,

    @SerializedName("icebreakers")
    @Expose
    var icebreakers: Any? = null,

    @SerializedName("story")
    @Expose
    var story: Any? = null,

    @SerializedName("meetup")
    @Expose
    var meetup: Any? = null,

    @SerializedName("verification_status")
    @Expose
    var verificationStatus: String? = null,

    @SerializedName("latest_poll")
    @Expose
    var latestPoll: Any? = null,

    @SerializedName("poll_info")
    @Expose
    var pollInfo: Any? = null,

    @SerializedName("has_active_subscription")
    @Expose
    var hasActiveSubscription: Boolean? = null,

    @SerializedName("show_concierge_badge")
    @Expose
    var showConciergeBadge: Boolean? = null,

    @SerializedName("lat")
    @Expose
    var lat: String? = null,

    @SerializedName("lng")
    @Expose
    var lng: String? = null,

    @SerializedName("last_seen")
    @Expose
    var lastSeen: String? = null,

    @SerializedName("online_code")
    @Expose
    var onlineCode: Int? = null,

    @SerializedName("profile_data_list")
    @Expose
    var profileDataList: List<ProfileData>? = null
)