package org.aisle.application.di.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import org.aisle.application.ui.views.main.*

@Module
abstract class MainFragmentBindingModule {
    @ContributesAndroidInjector
    abstract fun provideDiscoverFragment(): DiscoverFragment

    @ContributesAndroidInjector
    abstract fun provideNotesFragment(): NotesFragment

    @ContributesAndroidInjector
    abstract fun provideMatchesFragment(): MatchesFragment

    @ContributesAndroidInjector
    abstract fun provideProfileFragment(): ProfileFragment
}