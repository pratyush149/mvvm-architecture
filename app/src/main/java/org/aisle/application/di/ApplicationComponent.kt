package org.aisle.application.di

import android.app.Application
import org.aisle.application.base.BaseApplication
import org.aisle.application.di.module.ActivityBindingModule
import org.aisle.application.di.module.ApplicationModule
import org.aisle.application.di.module.ContextModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import dagger.android.support.DaggerApplication
import javax.inject.Singleton


@Singleton
@Component(modules = [ContextModule::class,AndroidSupportInjectionModule::class, ApplicationModule::class, ActivityBindingModule::class])
interface ApplicationComponent : AndroidInjector<DaggerApplication> {

    fun inject(app: BaseApplication)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder
        fun build(): ApplicationComponent
    }

}