package org.aisle.application.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import org.aisle.application.di.util.ViewModelFactory
import org.aisle.application.di.util.ViewModelKey
import org.aisle.application.ui.viewmodel.MainViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import org.aisle.application.ui.viewmodel.LoginViewModel
import org.aisle.application.ui.viewmodel.VerifyOtpViewModel
import javax.inject.Singleton

@Module
abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun bindMainViewModel(viewModel: MainViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    abstract fun bindLoginViewModel(viewModel: LoginViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(VerifyOtpViewModel::class)
    abstract fun bindVerifyOtpViewModel(viewModel: VerifyOtpViewModel): ViewModel

    @Binds
    @Singleton
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}