package org.aisle.application.utils

import android.content.Context
import org.aisle.application.data.prefs.MyPreferences
import javax.inject.Inject

class Session @Inject constructor(var context: Context) {

    fun getToken(): String? {
        return MyPreferences.getInstance(context).getString(SHARED_PREF_TOKEN, "")
    }

    fun setToken(token: String) {
        return MyPreferences.getInstance(context).saveString(SHARED_PREF_TOKEN, token)
    }

}